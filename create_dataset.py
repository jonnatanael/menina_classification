import glob, os, shutil
import random
random.seed(1234)

def main():
	data_path = "/opt/workspace/host_storage_hdd/menina/final_data/dataset/"
	out_path = "/opt/workspace/host_storage_hdd/menina/final_dataset_9_cls/"

	try:
		shutil.rmtree(out_path)
	except:
		pass

	try:
		os.mkdir(out_path)
	except:
		pass

	try:
		os.mkdir(out_path+'train')
	except:
		pass

	try:
		os.mkdir(out_path+'test')
	except:
		pass

	f = 0.8
	
	classes = glob.glob(data_path+'**')
	print(classes)

	for cl in classes:
		if cl.split('/')[-1]=='za_kape_170': # ignore last class
			continue
		print(cl)
		cl_nm = cl.split('/')[-1]
		print(cl_nm)
		cl_pth_train = out_path+'train/'+cl_nm+'/'
		cl_pth_test = out_path+'test/'+cl_nm+'/'

		try:
			os.mkdir(cl_pth_train)
		except:
			pass

		try:
			os.mkdir(cl_pth_test)
		except:
			pass

		images = glob.glob(cl+'/*.jpg')
		for fn in images:

			r = random.random()

			if r<f:
				# train
				shutil.copy(fn, cl_pth_train)
			else:
				# test
				shutil.copy(fn, cl_pth_test)

			# print(r, fn)

if __name__=="__main__":
	main()