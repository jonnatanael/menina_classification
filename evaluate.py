from __future__ import division
import json, os, re, sys, time, hashlib, ast, shutil
import numpy as np

import GPUtil

thr=0.1
GPUs = GPUtil.getGPUs()
r = [str(x) for x,y in enumerate(GPUs) if y.memoryUtil<thr] # find all free GPUs
r = r[0] # only use the first one available
r = ', '.join(r)

print("Using GPU: "+r)
os.environ['CUDA_VISIBLE_DEVICES'] = r


import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

import keras
from keras import backend as K
from keras.applications.imagenet_utils import preprocess_input
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
from keras.preprocessing import image
from glob import glob
from shutil import copyfile
from itertools import product
import functools

def zero_center(img):
	img = img-128
	return img

def w_categorical_crossentropy(y_true, y_pred):
	weights = U
	nb_cl = len(weights)
	final_mask = K.zeros_like(y_pred[:, 0])
	y_pred_max = K.max(y_pred, axis=1)
	y_pred_max = K.expand_dims(y_pred_max, 1)
	y_pred_max_mat = K.equal(y_pred, y_pred_max)
	for c_p, c_t in product(range(nb_cl), range(nb_cl)):
		final_mask += (K.cast(weights[c_t, c_p],K.floatx()) * K.cast(y_pred_max_mat[:, c_p] ,K.floatx())* K.cast(y_true[:, c_t],K.floatx()))
	return K.categorical_crossentropy(y_pred, y_true) * final_mask

def main():
	res_path = 'results/'
	# select model
	h = '006dc0f07cc9ff1e01b7319022b1c2c2'
	model_path = res_path+h+'/resnet50_best.h5'

	print(h)

	out_path = res_path+h+'/out/'

	try:
		os.mkdir(out_path)
	except:
		pass

	params_file = res_path+h+'/params.txt'
	f = open(params_file, "r")
	ln = f.readline()
	prm = ast.literal_eval(ln)

	SIZE = (prm['height'], prm['width']) # resized       
	dataset = prm['dataset']
	n_classes = prm['n_classes']

	if n_classes == 10:
		mapping = {0: 'II', 1: 'III', 2: 'IV', 3: 'I_B', 4: 'natur', 5: 'rustik', 6: 'rustik_plava', 7: 'svetli_H202', 8: 'temni_H202', 9: 'za_kape_170'}
		mapping_order = [4, 7, 8, 5, 6, 3, 0, 1, 2, 9]
		test_path = "/opt/workspace/host_storage_hdd/menina/final_dataset/test"
	else:
	# 9 classes
		mapping = {0: 'II', 1: 'III', 2: 'IV', 3: 'I_B', 4: 'natur', 5: 'rustik', 6: 'rustik_plava', 7: 'svetli_H202', 8: 'temni_H202'}
		mapping_order = [4, 7, 8, 5, 6, 3, 0, 1, 2]
		test_path = "/opt/workspace/host_storage_hdd/menina/final_dataset_9_cls/test"

	print(prm)

	t0 = time.time()
	model = load_model(model_path)
	t1 = time.time()
	print('Loaded in:', t1-t0)

	test_datagen = ImageDataGenerator(horizontal_flip=False,vertical_flip=False,rescale=1./255,preprocessing_function=zero_center)
	batchsize = 15

	test_generator = test_datagen.flow_from_directory(
	        test_path,
	        target_size=(SIZE[0], SIZE[1]),
	        batch_size=batchsize,
	        class_mode=None,
	        shuffle=False)

	# Get the filenames from the generator
	fnames = test_generator.filenames
	#print(fnames)
	 
	# Get the ground truth from generator
	ground_truth = test_generator.classes

	# Get the label to class mapping from the generator
	label2index = test_generator.class_indices
	 
	# Getting the mapping from class index to class label
	idx2label = dict((v,k) for k,v in label2index.items())
	print(idx2label)
	 
	# Get the predictions from the model using the generator
	predictions = model.predict_generator(test_generator, steps=test_generator.samples/test_generator.batch_size,verbose=1)
	predicted_classes = np.argmax(predictions,axis=1)
	predicted_classes_2 = np.argpartition(predictions, -2)[:,-2] # to get second best score
	 
	print(ground_truth)
	print(predicted_classes)

	ground_truth = [mapping_order.index(i) for i in ground_truth]
	predicted_classes = [mapping_order.index(i) for i in predicted_classes]

	print(ground_truth)
	print(predicted_classes)

	outfile = open(h+"_evaluation_results.txt", "w")

	#return
	rank2=True
	rank2 =False

	n = len(test_generator.class_indices)
	M = np.zeros((n,n))
	N = len(ground_truth)
	hit = 0

	for i, gt in enumerate(ground_truth):
		
		if rank2==True:

			if ground_truth[i] == predicted_classes[i]:
				M[ground_truth[i]][predicted_classes[i]]+=1
				hit+=1
			elif ground_truth[i] == predicted_classes_2[i]:
				M[ground_truth[i]][predicted_classes_2[i]]+=1
				hit+=1
			else:
				M[ground_truth[i]][predicted_classes[i]]+=1
		else:
			M[ground_truth[i]][predicted_classes[i]]+=1
			if ground_truth[i] == predicted_classes[i]:
				hit+=1
			else:
				# save file
				r1 = mapping[mapping_order[ground_truth[i]]]
				r2 = mapping[mapping_order[predicted_classes[i]]]
				print(fnames[i], r1, r2)
				shutil.copyfile(test_path+'/'+fnames[i], out_path+str(i)+'_'+r1+'_'+r2+'.jpg')

	outfile.write("overall accuracy: " + str(hit/N) + "\n")
	print(M)
	outfile.write("confusion matrix: \n")
	np.savetxt(outfile, M, fmt = '%i')

	P = []
	R = []
	F = []

	for i in range(0,len(M)):
		lbl = mapping[mapping_order[i]]
		TP = M[i,i]
		FP = sum(M[:,i])-TP
		FN = sum(M[i,:])-TP
		
		prec = TP/(TP+FP)
		rec = TP/(TP+FN)
		f = 2*(prec*rec)/(prec+rec)

		P.append(prec)
		R.append(rec)
		F.append(f)
		print(i, TP)
		print(lbl)

		outfile.write(lbl+", "+str(TP)+", "+str(FP)+", "+str(FN)+", "+str(prec)+", "+str(rec)+", "+str(f)+"\n")

	outfile.write("mean precision: "+ str(np.mean(P))+"\n")
	outfile.write("mean recall: " + str(np.mean(R))+"\n")
	outfile.write("mean F: " + str(np.mean(F))+"\n")

	outfile.close()

if __name__=='__main__':
	main()