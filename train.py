
# https://github.com/sebastianbk/finetuned-resnet50-keras

from __future__ import division
import numpy as np
import keras
import keras.backend as K
import math, json, os, sys, shutil, hashlib
from scipy import misc
import matplotlib.pyplot as plt
import GPUtil

thr=0.1
GPUs = GPUtil.getGPUs()
r = [str(x) for x,y in enumerate(GPUs) if y.memoryUtil<thr] # find all free GPUs
r = r[0] # only use the first one available
r = ', '.join(r)

# os.environ['CUDA_VISIBLE_DEVICES'] = '3'
print("Using GPU: "+r)
os.environ['CUDA_VISIBLE_DEVICES'] = r
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
session = tf.Session(config=config)

from keras.layers import Dense, AveragePooling2D, Flatten, Dropout, Input, GaussianNoise
from keras.models import Model
from keras_contrib.applications.resnet import  ResNet50, ResNet, bottleneck
from keras.optimizers import Adam, RMSprop
from keras.preprocessing.image import ImageDataGenerator
from keras.callbacks import EarlyStopping, ModelCheckpoint, LearningRateScheduler, TensorBoard, ReduceLROnPlateau

def save_obj(obj, name, output_path):
    with open(output_path + '/' + name + '.txt', 'w') as f:
        f.write(obj)

def zero_center(img):
	img = img-128
	#print(img)
	return img

def main():	

	BATCH_SIZE = 10
	n_classes = 9

	data_path = "/opt/workspace/host_storage_hdd/menina/final_dataset_9_cls/"

	prm = {'height':200, 'width':1000, 'lr':1e-5, 'epochs': 100,'dropout': -1, 'dataset':'original', 'n_classes': n_classes, 'arch':'keras_resnet50', 'flip': True, 'weights':'imagenet'}

	h = hashlib.md5(str(prm)).hexdigest()
	print(h)

	output_path = 'results/'

	if not os.path.isdir(output_path):
		os.mkdir(output_path)

	output_path+= h

	if os.path.isdir(output_path+h):
		raise OSError('The results directory already exists!')

	lr = prm['lr']
	NUM_EPOCHS = prm['epochs']
	SIZE = (prm['height'], prm['width']) # resized       
	arch = prm['arch']
	dataset = prm['dataset']
	n_classes = prm['n_classes']
	weights = prm['weights']
	flip = prm['flip']
	
	TRAIN_DIR=data_path+"train/"
	VAL_DIR=data_path+"test/"

	num_train_samples = sum([len(files) for r, d, files in os.walk(TRAIN_DIR)])
	num_train_steps = math.floor(num_train_samples/BATCH_SIZE)

	batches_train = ImageDataGenerator(horizontal_flip=flip,vertical_flip=flip,rescale=1./255,preprocessing_function=zero_center).flow_from_directory(TRAIN_DIR, target_size=SIZE, class_mode='categorical', shuffle=True, batch_size=BATCH_SIZE)
	batches_val = ImageDataGenerator(horizontal_flip=flip,vertical_flip=flip,rescale=1./255,preprocessing_function=zero_center).flow_from_directory(VAL_DIR, target_size=SIZE, class_mode='categorical', shuffle=True, batch_size=BATCH_SIZE)
	classes = list(iter(batches_train.class_indices))

	# define callbacks
	checkpointer = ModelCheckpoint(output_path+'/resnet50_best.h5', verbose=1, save_best_only=True, monitor='val_acc', mode='auto', period = 1)
	stop_callback = EarlyStopping(monitor='val_acc', patience=50, verbose=1, mode='auto')
	tbCallBack = TensorBoard(log_dir=output_path+'/', histogram_freq=0, write_graph=True, write_images=True)

	model = keras.applications.ResNet50(include_top=False, weights='imagenet', input_tensor=None, input_shape=(SIZE[0], SIZE[1], 3), pooling='avg', classes=n_classes)
	
	x = model.output
	x = Dense(n_classes, activation='softmax')(x)
	model = Model(inputs=model.input, outputs=x)

	model.compile(optimizer=Adam(lr=lr), loss='categorical_crossentropy', metrics=['accuracy'])

	# if model created and compiled properly, create results directory
	try:
		os.mkdir(output_path)
	except:
		pass
	save_obj(str(prm), 'params',output_path)
	
	model.fit_generator(batches_train, steps_per_epoch=num_train_steps, epochs=NUM_EPOCHS, callbacks=[checkpointer, tbCallBack],validation_data=batches_val)
	
	print('saving model to resnet50_final.h5')
	model.save(output_path+'/resnet50_final.h5')

if __name__=="__main__":
	main()