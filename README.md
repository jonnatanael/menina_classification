## Funkcionalnost

Skripta train.py naloži učne slike in nauči klasifikacijski model na podlagi Resnet50.
Skripta evaluate.py izračuna natačnost in F-score po posameznem razredu na testni množici.

## Knjižnice

- Keras 2.2.5
- Keras_contrib 0.0.2
- GPUtil 1.4.0

## Struktura
- train.py
- evaluate.py

## Slike
So na voljo na ViCoS boxu: [slike][data]

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [data]: <https://box.vicos.si/jonm/menina_final_dataset.zip>
   
